<?php

namespace Drupal\announcement_modal\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Announcement Settings.
 */
class AnnouncementSettings extends ConfigFormBase {

  /**
   * The config_factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructs a new AnnouncementSettings object.
   */
  public function __construct(ConfigFactory $config_factory, EntityStorageInterface $file_storage) {
    $this->configFactory = $config_factory;
    $this->fileStorage = $file_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('file')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'announcement.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'announcement_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('announcement.settings');
    $form['banner_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Announcements and Holidays'),
      '#description' => $this->t('Modal banner title for Announcements and Holidays'),
      '#default_value' => $config->get('banner_title'),
      '#required' => TRUE,
      '#maxlength' => 50,
    ];
    $form['banner_desc'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Announcements and Holidays Description'),
      '#description' => $this->t('Modal banner Description for Announcements and Holidays'),
      '#default_value' => $config->get('banner_desc.value'),
      '#format' => $config->get('banner_desc.format'),
      '#required' => TRUE,
      '#maxlength' => 1000,
    ];
    $form['show_banner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Toggle Announcement'),
      '#description' => $this->t('To enable and disable the Announcement modal.'),
      '#default_value' => $config->get('show_banner'),
    ];
    $form['background'] = [
      '#type' => 'details',
      '#title' => $this->t('Background'),
      '#collapsible' => TRUE,
      '#states' => [
        // Action to take.
        'visible' => [
          ':input[name="show_banner"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['background']['background_style'] = [
      '#type' => 'radios',
      '#options' => [
        'bg_color'   => $this->t('Background Color'),
        'bg_img' => $this->t('Background Image'),
      ],
      '#title' => $this->t('Background Style'),
      '#default_value' => $config->get('background_style'),
      '#description' => $this->t('To add the background style to the modal.'),
      '#required' => TRUE,
    ];
    $form['background']['background_color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Background Color'),
      '#collapsible' => TRUE,
      '#states' => [
        // Action to take.
        'visible' => [
          ':input[name="background_style"]' => [
            'value' => 'bg_color',
          ],
        ],
      ],
    ];
    $form['background']['background_color']['banner_bg'] = [
      '#type' => 'select',
      '#options' => [
        'black-bg' => 'black',
        'white-bg' => 'white',
        'blue-bg' => 'blue',
        'green-bg' => 'green',
        'darkcyan-bg' => 'darkcyan',
        'gray-bg' => 'gray',
        'steelblue-bg' => 'steelblue',
      ],
      '#default_value' => $config->get('banner_bg'),
    ];
    $form['background']['background_image'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Background Image'),
      '#collapsible' => TRUE,
      '#states' => [
        // Action to take.
        'visible' => [
          ':input[name="background_style"]' => [
            'value' => 'bg_img',
          ],
        ],
      ],
    ];
    $form['background']['background_image']['banner_img'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://images/',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_image_resolution' => ['640x400', '640x400'],
        'file_validate_size' => [25600000],
      ],
      '#default_value' => $config->get('banner_img'),
      '#description' => $this->t('Upload or select the banner background image with dimention 640x400.'),
    ];
    $form['background']['banner_text_style'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Banner Text Style'),
      '#collapsible' => TRUE,
    ];
    $form['background']['banner_text_style']['banner_text'] = [
      '#type' => 'select',
      '#options' => [
        'black-text' => 'black',
        'black-text-with-white-bg' => 'black with white bg',
        'white-text' => 'white',
        'white-text-with-black-bg' => 'white with black bg',
      ],
      '#title' => $this->t('Text color'),
      '#default_value' => $config->get('banner_text'),
      '#description' => $this->t('To change the banner text color in the modal.'),
    ];
    $form['date'] = [
      '#type' => 'details',
      '#title' => $this->t('Date Filter'),
      '#collapsible' => TRUE,
      '#states' => [
        // Action to take.
        'visible' => [
          ':input[name="show_banner"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['date']['from_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('From Date'),
      '#default_value' => new DrupalDateTime($config->get('from_date')),
      '#required' => TRUE,
    ];
    $form['date']['to_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('To Date'),
      '#default_value' => new DrupalDateTime($config->get('to_date')),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $from_date = strtotime((string) $form_state->getValue('from_date'));
    $to_date = strtotime((string) $form_state->getValue('to_date'));
    if ($from_date > $to_date) {
      $form_state->setErrorByName('from_date', $this->t('From date should be smaller than to date.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $this->config('announcement.settings')
      ->set('banner_title', $form_state->getValue('banner_title'))
      ->save();
    $this->config('announcement.settings')
      ->set('banner_desc.value', $values['banner_desc']['value'])
      ->save();
    $this->config('announcement.settings')
      ->set('banner_desc.format', $values['banner_desc']['format'])
      ->save();
    $this->config('announcement.settings')
      ->set('background_style', $form_state->getValue('background_style'))
      ->save();
    $this->config('announcement.settings')
      ->set('banner_bg', $form_state->getValue('banner_bg'))
      ->save();
    if ($fid = $form_state->getValue(['banner_img', 0])) {
      if (!empty($fid)) {
        $file = $this->fileStorage->load($fid);
        $file->setPermanent();
        $file->save();
      }
    }
    $this->config('announcement.settings')
      ->set('banner_img', $form_state->getValue('banner_img'))
      ->save();
    $this->config('announcement.settings')
      ->set('banner_text', $form_state->getValue('banner_text'))
      ->save();
    $this->config('announcement.settings')
      ->set('from_date', (string) $form_state->getValue('from_date'))
      ->save();
    $this->config('announcement.settings')
      ->set('to_date', (string) $form_state->getValue('to_date'))
      ->save();
    $this->config('announcement.settings')
      ->set('show_banner', $form_state->getValue('show_banner'))
      ->save();
  }

}
